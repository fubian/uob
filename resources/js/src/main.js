//
// Main JS
//
import HomeLib from "../libs/Home";

(function ($) {
    "use strict";

    $(document).ready(function ()
    {
        //Home script
        new HomeLib().init();
        //scrollreveral
        window.sr = ScrollReveal({
          reset: true,
          origin: 'bottom',
          distance: '20px',
          delay: '300',
          scale: '1',
          duration: 800, }, 100);
        sr.reveal('.foo');



        //sticky
       $(window).scroll(function (event) {
          var scroll = $(window).scrollTop();
          if (! $("#hidden-sticky").hasClass('closed')) {
          $('#hidden-sticky').toggleClass('showing-up',
          //add 'ok' class when div position match or exceeds else remove the 'ok' class.
          scroll >= $('#to-remove-sticky').offset().top

        );}
      });





       $( "#closesticky" ).click(function() {
          $("#hidden-sticky").removeClass('showing-up');
          $("#hidden-sticky").addClass('closed');
      });

        // Smoth scroll
        $('a[href*="#"]')
          // Remove links that don't actually link to anything
          .not('[href="#"]')
          .not('[href="#0"]')
          .click(function(event) {
            // On-page links
            if (
              location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
              &&
              location.hostname == this.hostname
            ) {
              // Figure out element to scroll to
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
              // Does a scroll target exist?
              if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                  scrollTop: target.offset().top
                }, 1000, function() {
                  // Callback after animation
                  // Must change focus!
                  var $target = $(target);
                  $target.focus();
                  if ($target.is(":focus")) { // Checking if the target was focused
                    return false;
                  } else {
                    $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                    $target.focus(); // Set focus again
                  };
                });
              }
            }
          });


        $('.menu-mobile.mobile li:first-child').addClass('active');
        $('.menu-mobile.mobile a').click(function()
        {
            let target = $(this).attr('href');
            if ($(target).length > 0)
            {
                $('.menu-mobile.mobile li').removeClass('active');
                $(this).parent().addClass('active');
                let left = $(target).position().left;
                let innerWidth = $('.wraper-inner-account-stash').innerWidth;
                $('.wraper-inner-account-stash .table-responsive').scrollLeft(left - innerWidth);
            }
            return false;
        });
    });
})(jQuery);
